package org.launchcode.training;


import org.junit.Test;
import static org.junit.Assert.*;

public class CarTest {
    //TODO: constructor sets gasTankLevel propery
    @Test
    public void defaultGasTankLevelIsCorrect() {
        Car test_car = new Car("Toyota", "Prius", 10, 50.0);
        assertEquals(10, test_car.getGasTankLevel(), .0001);

    }

    //TODO: gasTankLevel is accurate after driving within tank range
    @Test
    public void gasTankLevelAccurateAfterDriving() {
        Car test_car = new Car("Toyata", "Camry", 10, 25);
        test_car.drive(25);
        assertEquals(9, test_car.getGasTankLevel(), .0001);
    }
    //TODO: gasTankLevel is accurate after attempting to drive past tank range
    @Test
    public void gasTankAccurateAfterExceedingRange() {
        Car test_car = new Car("Toyota", "Prius", 10, 50);
        test_car.drive(500);
        assertEquals(0, test_car.getGasTankLevel(), .0001);
    }
    //TODO: can't have more gas than tank size, expect an exception
    @Test(expected = IllegalArgumentException.class)
    public void noTankOverfill() {
        Car test_car = new Car("Silly", "Car", 1, 1);
        test_car.addGas(1);
        fail("shouldn't get here, added gas beyond tank size");
    }


    // What are we testing? -> odometer
    // What is an odometer? -> keeps track of how many miles a car has driven
    // What happens when you create a new car? -> 0
    // Should odometer be a double or an integer?
    // If the stakeholders want odoemeter to be an integer how does int() round doubles?
    @Test
    public void testOdometerIsZero() {
        Car test_car = new Car("Toyota", "Prius", 10, 50);
        assertEquals(0, test_car.getOdometer(), .001);
    }

    @Test
    public void testOdodmeterAfterDriving() {
        Car test_car = new Car("Toyota", "Prius", 10, 50);
        test_car.drive(50);
        assertEquals(50, test_car.getOdometer(), .001);
    }

    //TODO: Test that (int) rounds doubles like 1.2 to 1

    //TODO: Test that (int) rounds doubles like 1.8 to 1

}
